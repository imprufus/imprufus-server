# frozen_string_literal: true

source 'https://rubygems.org'

# ruby '>= 2.3.0'

# [Core]
gem 'puma', '~> 3.0'
gem 'rails', '~> 5.0.1'

# [Database & Cache]
gem 'hiredis', '~> 0.6.1'
gem 'pg', '~> 0.19.0'
gem 'redis', '>= 3.2.0', require: ['redis', 'redis/connection/hiredis']
gem 'redis-rails'

# [Bootstrap]
gem 'autoprefixer-rails'
gem 'bootstrap-sass', '>= 3.0.0'
gem 'sass-rails', '~> 5.0'

# [JavaScript]
gem 'coffee-rails', '~> 4.2'
gem 'jbuilder', '~> 2.5'
gem 'jquery-rails'
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'

# [Auth]
gem 'bcrypt', '~> 3.1.7'

# [JSON API]
gem 'jsonapi-resources', '~> 0.8.1'
gem 'pundit-resources', '~> 1.1.1'

group :development, :test do
    gem 'rspec-rails', '~> 3.5.0'
    gem 'rubocop', '~> 0.46.0'
    gem 'rubocop-rspec', '~> 1.8.0'
end

group :development do
    gem 'listen', '~> 3.0.5'
    gem 'pry-byebug', platform: :mri
    gem 'pry-rails'
    gem 'spring'
    gem 'spring-watcher-listen', '~> 2.0.0'
end
