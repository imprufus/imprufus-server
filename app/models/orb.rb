class Orb < ApplicationRecord
    validates :name, presence: true
    validates :desc, presence: true

    belongs_to :user

    has_many :layers
    has_many :questions, through: :layers
end
