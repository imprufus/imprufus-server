class Layer < ApplicationRecord
    validates :name, presence: true

    belongs_to :user
    belongs_to :orb

    has_many :questions
end
