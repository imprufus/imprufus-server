class QuestionJSONValidator < ActiveModel::Validator
    def validate(record)
        case record.type
        when 'single_choice'
            record.errors[:base] << '"correct" field required' if record.payload[:correct].nil?
            record.errors[:base] << '"wrong" field required' if record.payload[:wrong].nil?
        when 'multiple_choice'
            record.errors[:base] << '"correct" field required' if record.payload[:correct].nil?
            record.errors[:base] << '"wrong" field required' if record.payload[:wrong].nil?
        when 'fill_in'
            record.errors[:base] << '"correct" field required' if record.payload[:correct].nil?
        when 'pairs'
            record.errors[:base] << '"correct" field required' if record.payload[:correct].nil?
        when 'assoc'
            record.errors[:base] << '"choices" field required' if record.payload[:choices].nil?
            record.errors[:base] << '"answers" field required' if record.payload[:answers].nil?
            record.errors[:base] << '"wrong" field required' if record.payload[:wrong].nil?
        when 'dropdown'
            record.errors[:base] << '"text" field required' if record.payload[:choices].nil?
        end
    end
end

class Question < ApplicationRecord
    validates :type,     presence: true
    validates :language, presence: true
    validates :info,     length: { minimum: 100 }

    validates_with QuestionJSONValidator

    belongs_to :user
    belongs_to :layer
    belongs_to :orb
end
