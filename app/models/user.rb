class User < ApplicationRecord
    validates :nickname, presence: true, uniqueness: true

    has_many :orbs
    has_many :layers, through: :orbs
    has_many :questions, through: :layers
end
