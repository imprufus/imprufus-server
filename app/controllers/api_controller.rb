class ApiController < JSONAPI::ResourceController
    include Pundit::ResourceController

    protect_from_forgery with: :null_session

    protected

    def current_user
        User.find(params[:id])
    end
end
