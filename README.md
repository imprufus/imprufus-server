# ImpRufus Server

[![build status](https://gitlab.com/valeth/imprufus-server/badges/master/build.svg)](https://gitlab.com/valeth/imprufus-server/commits/master)

## Setup

Run `bundle install --path vendor/bundle` to install all dependencies.

> Requires a running PostgreSQL server.

Then run `bin/rails server` to start the application locally.
