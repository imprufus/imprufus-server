Rails.application.routes.draw do
    concern :jsonapi_v1 do
        jsonapi_resources :users
        jsonapi_resources :orbs
        jsonapi_resources :layers
        jsonapi_resources :questions
    end

    scope :api do
        concerns :jsonapi_v1

        scope :v1 do
            concerns :jsonapi_v1
        end
    end

    concerns :app

    root 'root#index'
end
