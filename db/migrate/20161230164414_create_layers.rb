class CreateLayers < ActiveRecord::Migration[5.0]
    def change
        create_table :layers do |t|
            t.string :name
            t.string :topic
            t.string :language

            t.belongs_to :user
            t.belongs_to :orb

            t.timestamps
        end
    end
end
