class CreateQuestions < ActiveRecord::Migration[5.0]
    def change
        create_table :questions do |t|
            t.string :type
            t.string :language
            t.text   :info
            t.json   :payload

            t.belongs_to :user
            t.belongs_to :layer
            t.belongs_to :orb

            t.timestamps
        end
    end
end
